﻿using System;
using System.Collections.Generic;

namespace Tramigo.Tracking.DAL.Models
{
    public partial class Device
    {
        public Device()
        {
            Report = new HashSet<Report>();
        }

        public int DeviceId { get; set; }
        public string Name { get; set; }

        public ICollection<Report> Report { get; set; }
    }
}
