﻿using System;
using System.Collections.Generic;

namespace Tramigo.Tracking.DAL.Models
{
    public partial class Report
    {
        public int ReportId { get; set; }
        public int DeviceId { get; set; }
        public string Location { get; set; }
        public DateTime DateCreated { get; set; }

        public Device Device { get; set; }
    }
}
