using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tramigo.Tracking.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Tramigo.Tracking.BOL;

namespace TramigoApp.Controllers
{
    [Route("api/[controller]")]
    public class DeviceController : Controller
    {
        /// <summary>
        /// Get All Devices with Reports Included
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<DeviceModel>> GetDevices()
        {
            try
            {
                using (var db = new DeviceDBContext())
                {
                    var devices = await db.Device.Include(d=>d.Report).ToListAsync();
                    var model = devices.Select(d => d.ConvertToModel());
                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        //Get Reports by Max Date
        [HttpGet("[action]")]
        public IEnumerable<ReportModel> GetReportsMaxDate(DateTime maxDate)
        {
            try
            {
                using (var db = new DeviceDBContext())
                {
                    var reports = db.Report.Where(r=>r.DateCreated <= maxDate).ToList();
                    var model = reports.Select(r=>r.ConvertToModel());
                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Get Max Date Report
        [HttpGet("[action]")]
        public ReportModel GetMaxDateReport()
        {
            try
            {
                using (var db = new DeviceDBContext())
                {
                    var report = db.Report.OrderByDescending(r => r.DateCreated).FirstOrDefault();
                    var model = report.ConvertToModel();
                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
