import { DeviceModel } from "./DeviceModel";

export class ReportModel {
  reportId: number;
  deviceId: number;
  location: string;
  dateCreated: Date;
  dateCreatedText: string;
  device: DeviceModel;
}
