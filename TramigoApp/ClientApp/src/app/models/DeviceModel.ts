import { ReportModel } from "./ReportModel";

export class DeviceModel {
  deviceId: number;
  name: string;
  report: ReportModel[];
}
