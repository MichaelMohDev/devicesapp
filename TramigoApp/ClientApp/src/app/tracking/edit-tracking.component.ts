import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DeviceModel } from '../models/DeviceModel';

@Component({
  selector: 'app-edit-tracking',
  templateUrl: './edit-tracking.component.html'
})
export class EditTrackingComponent {
  public devices: DeviceModel[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<DeviceModel[]>(baseUrl + 'api/Device/GetDevices').subscribe(result => {
      this.devices = result;
      console.info(this.devices);
    }, error => console.error(error));
  }
}

