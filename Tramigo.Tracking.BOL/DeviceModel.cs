﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tramigo.Tracking.BOL
{
    public class DeviceModel
    {
        public DeviceModel()
        {
            Reports = new List<ReportModel>();
        }

        public int DeviceId { get; set; }
        public string Name { get; set; }

        public List<ReportModel> Reports { get; set; }
    }

    public partial class ReportModel
    {
        public int ReportId { get; set; }
        public int DeviceId { get; set; }
        public string Location { get; set; }
        public DateTime DateCreated { get; set; }
        public string DateCreatedText { get; set; }

        public DeviceModel Device { get; set; }
    }
}
