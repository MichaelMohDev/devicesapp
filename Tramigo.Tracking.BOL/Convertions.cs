﻿using System;
using System.Collections.Generic;
using System.Text;
using Tramigo.Tracking.DAL.Models;
using System.Linq;

namespace Tramigo.Tracking.BOL
{
    public static class Convertions
    {
        public static DeviceModel ConvertToModel(this Device device) {
            return new DeviceModel() {
                DeviceId = device.DeviceId,
                Name = device.Name,
                Reports = device.Report.Select(r => r.ConvertToModel()).ToList()
            };
        }

        public static ReportModel ConvertToModel(this Report report)
        {
            return new ReportModel()
            {
                ReportId = report.ReportId,
                DeviceId = report.DeviceId,
                DateCreated = report.DateCreated,
                DateCreatedText = report.DateCreated.ToShortDateString(),
                Location = report.Location

            };
        }
    }
}
